
# Own Open Stack - relay on Python, FLASK, TerraForm , Bash

**Project Goal:**

The goal of the project is to create a website to help users manage DataCebter resource. Possibility to create virtual machines on VMWare datacenter in an easy and accessible way.

As forendend will be used:
 - Flask
 - Python
 - HTML

As backend will be used:
 - Terraform
 - Bash
 
 
 Additional detailed info:
 
 https://github.com/ITAndreHoch/Terraform-stage-vmware
 https://github.com/ITAndreHoch/Docs-Flask
 
 
 
 #

**Final appearance and operation:**

1. Step one: Page - Authorisation (Only SSL)

<img src="images/flask1.png " alt="drawing" width="400"/>

2. Step two: Page - Input data

<img src="images/flask2.png " alt="drawing" width="400"/>

3. The site should contain a list of available items to choose from - for example, all possible networks

<img src="images/flask4.png " alt="drawing" width="400"/>

4. Step Three: On the screen shoud appear log from (terraform) deaployment new machine:
